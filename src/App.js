import React from 'react';
import MovieList from './components/MovieList';
import { BrowserRouter as Router, Route, Switch,Link } from 'react-router-dom';
import Home from './components/Home';
import MovieDetails from './components/MovieDetails';

function App() {
  return (
    <Router>
        <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/movie/:id" component={MovieDetails} />
        </Switch>
    </Router>
  )
}

export default App;
