import React from "react";
import {
    Navbar,
    Nav,
    Container,
} from "react-bootstrap";

const NavbarComponent = () => {
    return (
        <Navbar expand="lg" className="navbar" data-bs-theme="dark" >
            <Container style={{ margin : 0, paddingLeft : '30px'}}>
                <Navbar.Brand href="/"><h1>Movies</h1></Navbar.Brand>
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                    className="me-auto my-2 my-lg-0"
                    style={{ maxHeight: "100px" }}
                    navbarScroll
                    >
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};

export default NavbarComponent;
