// SortFilter.js
import React from 'react';
import {  Row, Col } from "react-bootstrap";

function SortFilter({ genres, selectedGenre, onSelectGenre, onSortChange, onSearch,handlePrevPage,handleNextPage,totalPages,page }) {
  return (
        <Row>
            <Col md={'auto'} style={{ padding : '0px',float : 'right'}}>
                <div>
                    <span className='text-white' style={{ fontSize : '24px',}}>Sort By</span>
                </div>
            </Col>
            <Col lg={2} md={2} xs={12} className='mt-1'>
                <select className='form-control' onChange={(e) => onSortChange(e.target.value)} style={{ backgroundColor : '#000', color : '#ffff'}}>
                    <option value="popularity.desc">Popularity Descending</option>
                    <option value="popularity.asc">Popularity Ascending</option>
                    <option value="vote_average.desc">Rating Descending</option>
                    <option value="vote_average.asc">Rating Ascending</option>
                    <option value="release_date.desc">Release Date Descending</option>
                    <option value="release_date.asc">Release Date Ascending</option>
                        {/* Add more sorting options */}
                </select>
            </Col>
            <Col md={'auto'} style={{ padding : '0px'}}>
                <div>
                    <span className='text-white' style={{ fontSize : '24px',}}>Filter</span>
                </div>
            </Col>
            <Col lg={2} md={2} xs={12} className='mt-1'>
                <select className='form-control' id="genre" value={selectedGenre} onChange={(e) => onSelectGenre(e.target.value)} style={{ backgroundColor : '#000', color : '#ffff'}}>
                    <option value="">All Genres</option>
                    {genres.map((genre) => (
                        <option key={genre.id} value={genre.id}>{genre.name}</option>
                    ))}
                </select>
            </Col>
            {/* <Col md={'auto'} style={{ padding : '0px'}}>
                <div>
                    <span className='text-white' style={{ fontSize : '24px',}}>Search</span>
                </div>
            </Col>
            <Col lg={2} md={2} xs={12} className='mt-1'>
                <input className='form-control' type="text" id="serch" placeholder="Search movies"  onChange={(e) => onSearch(e.target.value)}  style={{ backgroundColor : '#000', color : '#ffff'}} />
            </Col> */}
        </Row>
  );
}

export default SortFilter;
