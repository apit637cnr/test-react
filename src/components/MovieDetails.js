import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import NavbarComponent from './NavbarComponent';
import { Col, Row,Container } from "react-bootstrap";

function MovieDetails() {
  const { id } = useParams();
  const [movie, setMovie] = useState(null);

  useEffect(() => {
    const fetchMovie = async () => {
      const apiKey = '72efd70331dde0cb8e72bedf08122a8d';
      const response = await axios.get(
        `https://api.themoviedb.org/3/movie/${id}?api_key=${apiKey}`
      );
      setMovie(response.data);
    };

    fetchMovie();
  }, [id]);

  return (
      <div>
        <NavbarComponent />
        <Container fluid className='px-3 mt-5'>
            {movie && (
                <Row>
                    <Col md={4} xs={12}>
                        <img src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`} alt={movie.title} />
                    </Col>
                    <Col md={8} xs={12}>
                        <h1 className='text-white mt-md-0 mt-3'>{movie.title}</h1>
                        <div className='text-white mt-5'>
                            {movie.overview}
                        </div>
                    </Col>
                </Row>
            )}
        </Container>
    </div>
  );
}

export default MovieDetails;
