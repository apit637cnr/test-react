import React from 'react';
// import axios from 'axios';
import {  Link } from 'react-router-dom';
import { Col, Card, Row } from "react-bootstrap";

function MovieList({ movies }) {
    return (
        <>
            <Row>
                {movies.map((movie) => (
                    <Col className="mb-5 mt-5" lg={2} md={6} xs={6} key={movie.id}>
                        <Link to={`/movie/${movie.id}`} style={{ textDecoration: 'none' }}>
                            <Card>
                                <Card.Img
                                    variant="top"
                                    style={{ maxHeight: '25rem' }}
                                    src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`} alt={movie.title}
                                />
                            </Card>
                            <Card.Body style={{ marginTop: '10px' }}>
                                <Card.Title><h5 style={{ color: 'white' }}>{movie.title}</h5></Card.Title>
                                <Card.Text><span style={{ color: 'white', fontSize: '14px' }}>{movie.overview.slice(0, 50) + '..'}</span></Card.Text>
                            </Card.Body>
                        </Link>
                    </Col>
                ))}
            </Row>
        </>
    );
}

export default MovieList;
