import React, { useState, useEffect } from 'react';
import MovieList from './MovieList';
import { Col, Row,Container } from "react-bootstrap";
import NavbarComponent from './NavbarComponent';
import axios from 'axios';
import SortFilter from './SortFilter';

function Home() {
    const [movies, setMovies] = useState([]);
    const [genres, setGenres] = useState([]);
    const [selectedGenre, setSelectedGenre] = useState('');
    const [sortBy, setSortBy] = useState('popularity.desc');
    const [searchTerm, setSearchTerm] = useState('');
    const [page, setPage] = useState(1);
    const [totalPages, setTotalPages] = useState(0);

    useEffect(() => {
        const fetchMovies = async () => {
            const apiKey = '72efd70331dde0cb8e72bedf08122a8d';
            let url = `https://api.themoviedb.org/3/discover/movie?api_key=${apiKey}&sort_by=${sortBy}&page=${page}`;
            if (selectedGenre) {
                url += `&with_genres=${selectedGenre}`;
            }
            if (searchTerm) {
                url += `&query=${searchTerm}`;
            }
            const response = await axios.get(url);
            setMovies(response.data.results);
            setTotalPages(response.data.total_pages);
        };

        fetchMovies();
    }, [selectedGenre, sortBy, searchTerm, page]);

    useEffect(() => {
        const fetchGenres = async () => {
            const apiKey = '72efd70331dde0cb8e72bedf08122a8d';
            const response = await axios.get(`https://api.themoviedb.org/3/genre/movie/list?api_key=${apiKey}`);
            setGenres(response.data.genres);
        };

        fetchGenres();
    }, []);
    
    const handleSelectGenre = (genreId) => {
        setSelectedGenre(genreId);
    };

    const handleSortChange = (value) => {
        setSortBy(value);
    };

    const handleSearch = (value) => {
        setSearchTerm(value);
    };

    const handleNextPage = () => {
        setPage((prevPage) => prevPage + 1);
    };

    const handlePrevPage = () => {
        setPage((prevPage) => prevPage - 1);
    };

  return (
    <div className="App">
        <NavbarComponent />
            <div className="mt-1">
            <Row>
                <Col lg={12} md={6} xs={12}>
                    <Container fluid  className='mt-5 px-5' >
                        <SortFilter
                            genres={genres}
                            selectedGenre={selectedGenre}
                            onSelectGenre={handleSelectGenre}
                            onSortChange={handleSortChange}
                            onSearch={handleSearch}
                            prevPage={handlePrevPage}
                            nextPage={handleNextPage}
                            totalPages={totalPages}
                            page={page}
                        />
                     </Container>
                </Col>
            </Row>
            <Row>
                {/* <Col lg={4} md={6} sm={4} xs={12}> */}
                    <Container fluid  className='mt-1 px-5' >
                        <MovieList
                            movies={movies}
                            genres={genres}
                            selectedGenre={selectedGenre}
                            onSelectGenre={handleSelectGenre}
                            onSortChange={handleSortChange}
                            onSearch={handleSearch}
                            prevPage={handlePrevPage}
                            nextPage={handleNextPage}
                            totalPages={totalPages}
                            page={page}
                        />
                    </Container>
                {/* </Col> */}
            </Row>
            <div className='d-flex justify-content-center mb-5'>
                <div>
                    <button onClick={handlePrevPage} disabled={page === 1}>
                        Previous
                    </button>
                    <button onClick={handleNextPage} disabled={page === totalPages}>
                        Next
                    </button>
                </div>
            </div>
        </div>
    </div>
  );
}

export default Home;
